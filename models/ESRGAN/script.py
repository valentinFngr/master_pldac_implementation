import os 
import config 
import random as random 
import cv2
import numpy as np 


def split_train_to_val(val_size): 
    """
        check for existing val data. 
        If val data is not find, take a  
    """

    # check if val folder exists 
    if not os.path.exists(config.VAL_PATH): 
        os.mkdir(config.VAL_PATH)

    nb_val_files = len(os.listdir(config.VAL_PATH)) 
    print(f"Found {nb_val_files} in validation folder at : {config.VAL_PATH}")

    if nb_val_files != 0: 
        return 
    else: 
        train_files = os.listdir(config.TRAIN_PATH)
        random.shuffle(train_files)
        nb_files = len(train_files)
        val_len = int(nb_files * val_size)
        
        for path in train_files[:val_len]: 
            os.rename(
                os.path.join(config.TRAIN_PATH, path), 
                os.path.join(config.VAL_PATH, path)
            )

    return 





def save_image_to_folder(lr, sr, hr, epoch, experiment_name, mode="gan"): 
    """
        given a folder_path, save the image in this folder
    """

    lr_list = []
    sr_list = []

    # check if reuslt/validation exists
    if not os.path.exists(config.RESULT_PATH): 
        # create directory 
        os.mkdir(config.RESULT_PATH)
        os.mkdir(config.VAL_RESULT_PATH)
        os.mkdir(config.GAN_VAL_PATH)
        os.mkdir(config.PSNR_VAL_PATH)
            
    if mode == "psnr": 
        folder_path = os.path.join(config.PSNR_VAL_PATH, f"{epoch}_epoch")
    elif mode == "gan": 
        folder_path = os.path.join(config.GAN_VAL_PATH, f"{epoch}_epoch")

    print("creating fodler : ", folder_path)
    os.mkdir(folder_path)

    size = lr.shape[0]
    for i in range(size): 
        lr_img = np.transpose(lr[i].cpu().numpy(), (1, 2, 0))
        sr_img = np.transpose(sr[i].cpu().numpy(), (1, 2, 0))
        hr_img = np.transpose(hr[i].cpu().numpy(), (1, 2, 0))

        print(f"--- saving validation image {i} ---")
        lr_path = os.path.join(folder_path, f"validation_at_{epoch}_exp_{experiment_name.split('/')[1]}_lr{i}.jpg")
        sr_path = os.path.join(folder_path, f"validation_at_{epoch}_exp_{experiment_name.split('/')[1]}_sr{i}.jpg")
        hr_path = os.path.join(folder_path, f"validation_at_{epoch}_exp_{experiment_name.split('/')[1]}_hr{i}.jpg")

        cv2.imwrite(lr_path, lr_img)
        cv2.imwrite(sr_path, sr_img)
        cv2.imwrite(hr_path, hr_img)
        print("--- saving validation image : DONE ---")

    return 

