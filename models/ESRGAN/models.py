import config
import torch.nn as nn 
import torch 
import numpy as np 
import torchvision
import os 
os.environ['TORCH_HOME'] = '/Vrac/vgg_torch'
# from torchsummary import summary

class ResidualInResidualDenseBlock(nn.Module): 
    """
    ***************************************************************************************
    *    Title: Enhanced Super-Resolution Generative Adversarial Networks
    *    Author: Xintao Wang et al
    *    Date: 17 Sep 2018   
    *    Paper version: v2
    *    Availability: https://arxiv.org/pdf/1809.00219.pdf
    ***************************************************************************************  
    """

    def __init__(self): 
        super(ResidualInResidualDenseBlock, self).__init__()
        self.conv1 = nn.Conv2d(64, 64, 3, 1, 1, bias=False)
        self.conv2 = nn.Conv2d(128, 64, 3, 1, 1, bias=False)
        self.conv3 = nn.Conv2d(256, 64, 3, 1, 1, bias=False)
        self.conv4 = nn.Conv2d(512, 64, 3, 1, 1, bias=False)
        self.conv5 = nn.Conv2d(448, 64, 3, 1, 1, bias=False)
        self.LRElu = nn.LeakyReLU(config.lrelu_slope)

    def forward(self, inputs): 
        out1 = self.LRElu(self.conv1(inputs))
        out1_2 = torch.cat([inputs, out1], dim=1)
        out2 = self.LRElu(self.conv2(out1_2))
        out2_2 = torch.cat([inputs, out1_2, out2], dim=1)
        out3 = self.LRElu(self.conv3(out2_2))
        out3_2 = torch.cat([inputs, out1_2, out2_2, out3], dim=1)
        out4 = self.LRElu(self.conv4(out3_2))
        out4_2 = torch.cat([inputs, out1_2, out2_2], dim=1)
        out5 = self.conv5(out4_2) 

        return out5 * config.residual_scaling



class PixelShuffleBlock(torch.nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Real-Time Single Image and Video Super-Resolution Using an Efficient Sub-Pixel Convolutional Neural Network
    *    Author: Wenzhe Shi, Jose Caballero1 et al.
    *    Date: 2016
    *    Paper version: v2
    *    Availability: https://arxiv.org/pdf/1609.05158.pdf
    *
    ***************************************************************************************/
    """
    def __init__(self, in_chan, out_chan): 
        self.in_chan = in_chan
        self.out_chan = out_chan
        super(PixelShuffleBlock, self).__init__()
        
        self.conv = nn.Conv2d(in_chan, out_chan, 3, 1, 0, bias=False)
        self.pixelshuffler = torch.nn.PixelShuffle(2)
        self.prelu = nn.PReLU()
        
    def forward(self, x): 
        output = self.conv(x)
        output = self.prelu(self.pixelshuffler(output))
        return output



class ResidualBlock(torch.nn.Module): 
    """
        FROM SRGAN
    """
    def __init__(self, in_chan): 
        super(ResidualBlock, self).__init__()
        self.in_chan = in_chan
        self.conv1 = torch.nn.Conv2d(in_chan, 64, 3, stride=1, padding=1, bias=False)
        self.bn1 = torch.nn.BatchNorm2d(64) 
        self.prelu1 = torch.nn.PReLU()
        
        self.conv2 = torch.nn.Conv2d(in_chan, 64, 3, stride=1, padding=1, bias=False)
        self.bn2 = torch.nn.BatchNorm2d(64) 
        
        
    def forward(self, x): 
        
        # no copy needed
        residual = x
        
        out = self.prelu1(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        result = out + x
        return result
    

class DBlock(torch.nn.Module): 
    """
    SRGAN
    """
    def __init__(self, in_chan, out_chan, stride=1): 
        self.in_chan = in_chan 
        self.out_chan = out_chan
        self.stride = stride
        super(DBlock, self).__init__()
        self.conv = torch.nn.Conv2d(in_chan, out_chan, 3, stride=stride, bias=False)
        self.bn = torch.nn.BatchNorm2d(out_chan)
        self.lrelu = torch.nn.LeakyReLU(config.slope, inplace=True)
        
        
    def forward(self, x): 
        
        output = self.lrelu(self.bn(self.conv(x)))
        return output
                              
        
class PixelShuffleBlock(torch.nn.Module): 
    
    def __init__(self, in_chan, out_chan): 
        self.in_chan = in_chan
        self.out_chan = out_chan
        super(PixelShuffleBlock, self).__init__()
        
        self.conv = torch.nn.Conv2d(in_chan, out_chan, 3, stride=1, padding=0, bias=False)
        self.pixelshuffler = torch.nn.PixelShuffle(2)
        self.prelu = torch.nn.PReLU()
        
    def forward(self, x): 
        output = self.conv(x)
        output = self.prelu(self.pixelshuffler(output))
        return output


# class Generator(torch.nn.Module): 
    
#     def __init__(self): 
#         super(Generator, self).__init__() 
        
#         self.conv1 = torch.nn.Conv2d(3, 64, 3, stride=1, padding=3, dilation=1, bias=False)
#         self.prelu1 = torch.nn.PReLU()
        
#         self.residual_blocks = torch.nn.Sequential(*[
#             ResidualBlock(64).to(config.device) for _ in range(16)
#         ])
        
#         self.conv2 = torch.nn.Conv2d(64, 64, 3, stride=1, padding=1, bias=False)
#         self.bn1 = torch.nn.BatchNorm2d(64)
        
        
#         self.pxshuffle1 = PixelShuffleBlock(64, 256).to(config.device)
#         self.pxshuffle2 = PixelShuffleBlock(64, 256).to(config.device)
        
#         self.conv3 = torch.nn.Conv2d(64, 3, 5, stride=1, padding=0, bias=False)
#         self.tanh = torch.nn.Tanh()

#         self.apply(self._initialize_weights)
        

#     def forward(self, x):   
#         out1 = self.prelu1(self.conv1(x))
#         out2 = self.residual_blocks(out1)
#         out3 = self.bn1(self.conv2(out2))
#         out3 = out3 + out1
        
#         out4 = self.pxshuffle1(out3)
#         out5 = self.pxshuffle2(out4)
#         out6 = self.tanh(self.conv3(out5))
        
#         # super resolution image
#         assert out6.shape[1:] == (3, config.hr_size, config.hr_size)
#         return out6


#     def _initialize_weights(self, module):
#         """
#             He initialization, mentioned as MSRA initialization in the paper
#         """
#         print("--- weights initialization ---")
#         if isinstance(module, nn.Conv2d):
#             n = module.in_channels * module.out_channels
#             module.weight.data.normal_(0, np.sqrt(2. / n))
#             if module.bias is not None:
#                 module.bias.data.zero_()
#         elif isinstance(module, nn.Linear):
#             module.weight.data.normal_(0, 0.001)
#             module.bias.data.zero_()


class Generator(nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network
    *    Author: Christian Ledig, Lucas Theis et al.
    *    Date: 25 May 2017
    *    Paper version: v5
    *    Availability: https://arxiv.org/pdf/1609.04802.pdf
    *
    ***************************************************************************************/
    """

    def __init__(self): 
        super(Generator, self).__init__() 
        
        self.conv1 = nn.Conv2d(3, 64, 9, 1, 6, bias=False)
        self.prelu1 = nn.PReLU()

        rrdb_blocks = [
            ResidualBlock(64).to(config.device) for i in range(config.nb_rrdb_blocks)
        ]

        self.rrdb_blocks = nn.Sequential(*rrdb_blocks)

        self.conv2 = nn.Conv2d(64, 64, 3, 1, 1, bias=False) 
        # the only batch norm layer in the network 
        self.bn1 = nn.BatchNorm2d(64) 

        pxshuffler_blocks = [
            PixelShuffleBlock(64, 256) for i in range(2)
        ] 
        self.pxshuffler_blocks = nn.Sequential(*pxshuffler_blocks)

        self.conv3 = nn.Conv2d(64, 3, 5, stride=1, padding=0,bias=False)
        self.tanh = nn.Tanh()
        self.apply(self._initialize_weights)



    def forward(self, inputs): 
        out1 = self.prelu1(self.conv1(inputs))
        out2 = self.rrdb_blocks(out1)
        out3 = self.bn1(self.conv2(out2))   
        out3 = torch.add(out3, out1)
        out4 = self.pxshuffler_blocks(out3)
        out5 = self.tanh(self.conv3(out4))
        # super resolution image    
        assert out5.shape[1:] == (3, config.hr_size, config.hr_size)
        return out5

    def _initialize_weights(self, module):
        """
            He initialization, mentioned as MSRA initialization in the paper
        """
        if isinstance(module, nn.Conv2d):
            n = module.in_channels * module.out_channels
            module.weight.data.normal_(0, np.sqrt(2. / n))
            if module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.Linear):
            module.weight.data.normal_(0, 0.001)
            module.bias.data.zero_()


class DBlock(torch.nn.Module): 
    def __init__(self, in_chan, out_chan, stride=1): 
        self.in_chan = in_chan 
        self.out_chan = out_chan
        self.stride = stride
        super(DBlock, self).__init__()
        self.conv = torch.nn.Conv2d(in_chan, out_chan, 3, stride=stride, padding=0, bias=False)
        self.bn = torch.nn.BatchNorm2d(out_chan)
        self.lrelu = torch.nn.LeakyReLU(config.lrelu_slope, inplace=True)
        
        
    def forward(self, x): 
        
        output = self.lrelu(self.bn(self.conv(x)))
        return output


class Discriminator(torch.nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network
    *    Author: Christian Ledig, Lucas Theis et al.
    *    Date: 25 May 2017
    *    Paper version: v5
    *    Availability: https://arxiv.org/pdf/1609.04802.pdf
    *
    ***************************************************************************************/
    """
    def __init__(self): 
        super(Discriminator, self).__init__()
        self.conv1 = torch.nn.Conv2d(3, 32, 9, stride=1, padding=0, bias=False)
        self.lrelu1 = torch.nn.LeakyReLU(config.lrelu_slope, inplace=True)
        
        self.d_modules = torch.nn.Sequential(*[
            DBlock(32, 32, 2), 
            DBlock(32, 64, 1), 
            DBlock(64,64, 2), 
            DBlock(64, 128, 1), 
            DBlock(128, 128, 2), 
            DBlock(128, 256, 1),
            DBlock(256, 256, 2)
        ])
        
        self.fc1 = torch.nn.Linear(4096, 1024)
        self.lrelu2 = torch.nn.LeakyReLU(config.lrelu_slope, inplace=True)
        self.fc2 = torch.nn.Linear(1024, 1)
            
    def forward(self, x): 
        out1 = self.lrelu1(self.conv1(x))
        out2 = self.d_modules(out1).view(out1.shape[0], -1)
        # print(out2.shape)
        out3 = self.fc1(out2) 
        out4 = self.lrelu2(out3)
        out5 = self.fc2(out4)
        return out5

# class Discriminator(torch.nn.Module): 
#     """
#         FROM SRGAN
#     """
#     def __init__(self): 
#         super(Discriminator, self).__init__()
#         self.conv1 = torch.nn.Conv2d(3, 32, 9, stride=1, padding=1, bias=False)
#         self.lrelu1 = torch.nn.LeakyReLU(config.lrelu_slope, inplace=True)
        
#         self.d_modules = torch.nn.Sequential(*[
#             DBlock(32, 32, 2), 
#             DBlock(32, 64, 1), 
#             DBlock(64,64, 2), 
#             DBlock(64, 128, 1), 
#             DBlock(128, 128, 2), 
#             DBlock(128, 256, 1),
#             DBlock(256, 256, 2)
#         ])
        
#         self.fc1 = torch.nn.Linear(4096, 1024)
#         self.lrelu2 = torch.nn.LeakyReLU(config.lrelu_slope, inplace=True)
#         self.fc2 = torch.nn.Linear(1024, 1)
#         self.sigmoid = torch.nn.Sigmoid()
            
#     def forward(self, x): 
#         out1 = self.lrelu1(self.conv1(x))
#         out2 = self.d_modules(out1).view(out1.shape[0], -1)
#         out3 = self.fc1(out2) 
#         out4 = self.lrelu2(out3)
#         out5 = self.fc2(out4)
#         out6 = self.sigmoid(out5)
        
#         return out6

class PerceptualLoss(torch.nn.Module): 
    """
        Implementation of the content loss from the SRGAN paper
        We load a pre trained vgg19 and freeze all weights.
        We use the original high resolution image with the super resolution (generated)
        image and compare the mse error between their output when fed to the pre trained vgg19
    """
    def __init__(self):
        super(PerceptualLoss, self).__init__()
        # build pretrained vgg 
        # important : we extract the 35th layer as it is the one BEFORE activation. 
        # in SRGAN, we used the layer AFTER activation (cf srgan in this repository)
        layers = torch.nn.Sequential(*list(torchvision.models.vgg19(pretrained=True).eval().features.children())[:35])            
        # freeze all weights
        for param in layers.parameters():
            param.requires_grad = False
            
        self.layers = layers
        self.normalizations =  torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

        # mse criterion 
        self.mse_criterion = torch.nn.MSELoss()
        
    def forward(self, hr, sr): 
        hr_norm = self.normalizations(hr) 
        sr_norm = self.normalizations(sr)
        vgg_hr = self.layers(hr_norm) 
        vgg_sr = self.layers(sr_norm)
        result = self.mse_criterion(vgg_hr, vgg_sr) * config.rescaling_factor
        return result
