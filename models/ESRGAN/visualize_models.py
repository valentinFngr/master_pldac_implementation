from models import Generator, Discriminator 
from torchsummary import summary
import config

def main():
    g_net = Generator().to(config.device)
    summary(g_net, (3, config.lr_size, config.lr_size))
    print()
    d_net = Discriminator().to(config.device)
    summary(d_net, (3, config.hr_size, config.hr_size))


if __name__ == "__main__": 
    main()

