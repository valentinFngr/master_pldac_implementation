import torch

def compute_psnr(sr, hr): 
    """
        compute psnr between super resolution and ground truth high resolution
    """
    # applies the reduction
    mse = torch.nn.functional.mse_loss(sr*255.0, hr*255.0)
    psnr = 10*torch.log(255.0**2 / mse)
    return psnr
