from torch.utils.data import Dataset
import glob
from torchvision import transforms
from PIL import Image
import numpy as np
from torch.utils.data.dataloader import default_collate
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def my_collate(batch):
    batch = list(filter(lambda x:x is not None, batch))
    return default_collate(batch)


class ESRGANDataset(Dataset): 
    """
        A custom class to extract data
    """
    def __init__(self, hr_size, lr_size, folder, convert_to_rgb=True): 
        super(ESRGANDataset, self).__init__() 
        self.hr_size = hr_size 
        self.lr_size = lr_size
        self.folder = folder 
        self.convert_to_rgb = convert_to_rgb
    
        self.hr_transform = transforms.Compose([
            transforms.Resize((hr_size, hr_size)), 
            transforms.ToTensor(), 
            transforms.Normalize((0.5, ), (0.5,))
        ])
        
        self.lr_transform = transforms.Compose([
            transforms.Resize((lr_size, lr_size)), 
            transforms.ToTensor(), 
            transforms.Normalize((0.5, ), (0.5,))
        ])

        self.files = sorted(glob.glob(folder + "/*.*"))
        
    def __getitem__(self, idx): 
        
        img_name = self.files[idx] 
        try: 

            image_obj = Image.open(img_name)
            if self.convert_to_rgb: 
                image_obj = image_obj.convert("RGB")
            
            return {
                "hr" : self.hr_transform(image_obj), 
                "lr" : self.lr_transform(image_obj)
            }
        except Exception as e:
            print(e)
            return None
    
    def __len__(self): 
        return len(self.files)

