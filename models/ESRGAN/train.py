from pickle import TRUE
from tkinter.tix import Tree
import numpy as np 
import torch
import torch.nn as nn
import torchvision
import os 
import matplotlib.pyplot as plt
import config
from models import Discriminator, Generator, PerceptualLoss  
from dataset import ESRGANDataset, my_collate
from script import split_train_to_val, save_image_to_folder
from torch.utils.data import DataLoader
from utils import compute_psnr
from torch.utils.tensorboard import SummaryWriter
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


torch.manual_seed(0)

def get_models(): 
    """
        return models 
    """

    d_net = Discriminator().to(config.device) 
    g_net = Generator().to(config.device)

    if config.g_weight_path: 
        print("Generator Initialized from ", config.g_weight_path)
        g_net.load_state_dict(torch.load(config.g_weight_path))
    if config.d_weight_path: 
        print("Discriminator Initialized from ", config.g_weight_path)
        d_net.load_state_dict(torch.load(config.g_weight_path))

    return g_net, d_net 


def get_criterions(): 
    """
        return perpectual criterion, adversarial criterion, l1 criterion
    """

    percept = PerceptualLoss().to(config.device)
    adv = nn.BCELoss().to(config.device)
    l1 = nn.L1Loss().to(config.device)

    return percept, adv, l1


def get_dataloader(): 
    
    split_train_to_val(config.val_size)

    train_dataset = ESRGANDataset(config.hr_size, config.lr_size, folder=config.TRAIN_PATH)
    test_dataset = ESRGANDataset(config.hr_size, config.lr_size, folder=config.VAL_PATH)

    print(f"Found {len(train_dataset)} data points in Train")
    print(f"Found {len(test_dataset)} data points in Test")

    train_data = DataLoader(
        train_dataset, 
        batch_size=config.batch_size, 
        shuffle=True, 
        collate_fn=my_collate
    )

    test_data = DataLoader(
        test_dataset, 
        batch_size=config.batch_size, 
        shuffle=True, 
        collate_fn=my_collate
    )

    return train_data, test_data


def get_optimizers(g_net, d_net): 
    g_optim = torch.optim.Adam(g_net.parameters(), lr=config.learning_rate, betas=(0.9, 0.999))    
    g_scheduler = torch.optim.lr_scheduler.StepLR(g_optim, step_size=2*1e4, gamma=0.5, verbose=False)

    d_optim = torch.optim.Adam(d_net.parameters(), lr=config.learning_rate, betas=(0.9, 0.999))    
    d_scheduler = torch.optim.lr_scheduler.StepLR(d_optim, step_size=2*1e4, gamma=0.5, verbose=False)

    return g_optim, g_scheduler, d_optim, d_scheduler 

torch.autograd.set_detect_anomaly(True)
def train(
    g_net,
    d_net, 
    g_optim,
    d_optim,  
    g_scheduler,
    d_scheduler, 
    percept_criterion, 
    adv_criterion, 
    l1_criterion, 
    train_loader, 
    writer, 
    epoch
    ):

    d_net.train()
    g_net.train()
    
    print("identified batches : ", len(train_loader))
    for i, batch in enumerate(train_loader): 

        ##### Load data #####

        lr, hr = batch["lr"], batch["hr"]
        lr = lr.to(config.device)
        hr = hr.to(config.device)
        bs = lr.shape[0]

        #### generate images ####
        sr = g_net(lr)


        for p in d_net.parameters():
            p.requires_grad = True

        #### real predictions ####
        label = torch.full((bs, 1), 1, device=config.device, dtype=torch.float32)
        d_optim.zero_grad()
        real_pred = d_net(hr)
        fake_pred = d_net(sr.detach())
        real_pred = torch.sigmoid(real_pred - fake_pred.mean()) # relativistic
        fake_pred = torch.sigmoid(fake_pred - real_pred.mean()) # relativistic
        real_loss = adv_criterion(real_pred,label)
        real_loss.backward(retain_graph=True)
        #### predictions on generated images #### 
        label.fill_(0.0)
        fake_loss = adv_criterion(fake_pred, label)
        fake_loss.backward()
        #### compute total loss for the discriminator ####
        d_loss = fake_loss + real_loss
        #### update discriminator ####
        d_optim.step()
        d_scheduler.step()


        #### train generator ####
    
        for p in d_net.parameters():
            p.requires_grad = False
            
        g_net.zero_grad()
        label.fill_(1.)
        sr_preds = d_net(sr)
        # adversarial loss for generator
        g_adv_err = adv_criterion(torch.sigmoid(sr_preds - real_pred.detach().mean()), label) * config.lambda_coeff
        # perceptual loss
        g_perc_err = percept_criterion(hr.detach(), sr) 
        # l1 loss 
        g_l1_err = l1_criterion(hr.detach(), sr) * config.l1_coeff
        g_loss = g_adv_err + g_perc_err + g_l1_err
        g_loss.backward()
        g_optim.step()
        g_scheduler.step()

        with torch.no_grad(): 

            pred_sr = fake_pred.mean()
            pred_hr = real_pred.mean()

            writer.add_scalar("GAN_MODE/Train/D_loss", d_loss.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/G_loss", g_loss.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/G_perc", g_perc_err.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/G_adv", g_adv_err.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/G_l1", g_l1_err.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/D(HR)", pred_hr.item(), epoch*len(train_loader) + i + 1)
            writer.add_scalar("GAN_MODE/Train/D(SR)", pred_sr.item(), epoch*len(train_loader) + i + 1)
            
            # for n, p in g_net.named_parameters():
            #     if(p.requires_grad) and ("bias" not in n):
            #         # writer.add_histogram(f"GAN_MODE/GENERATOR/{n}", p.grad.cpu(), epoch*len(train_loader) + i + 1)
            #         # writer.add_scalar(f"GAN_MODE/GRAD/{n}", p.grad.data.norm(2).item(), epoch*len(train_loader) + i + 1)


            # for n, p in d_net.named_parameters():
            #     if(p.requires_grad) and ("bias" not in n):
            #         # writer.add_histogram(f"GAN_MODE/GENERATOR/{n}", p.grad.cpu(), epoch*len(train_loader) + i + 1)
            #         writer.add_scalar(f"GAN_MODE/GRAD/{n}", p.grad.data.norm(2).item(), epoch*len(train_loader) + i + 1)



            # if epoch % 30 == 0 and i == 0: 
            #     pred = g_net(lr)
            #     grid = torchvision.utils.make_grid(pred.cpu())
            #     grid = np.transpose(grid, (1,2, 0))
            #     plt.imshow(grid)
            #     plt.show()

            #     grid = torchvision.utils.make_grid(hr.cpu())
            #     grid = np.transpose(grid, (1,2, 0))
            #     plt.imshow(grid)
            #     plt.show()



def validate(
    g_net,
    d_net, 
    percept_criterion, 
    adv_criterion, 
    l1_criterion, 
    val_loader, 
    writer, 
    epoch
    ):

    total_psnr = []
    total_percept = [] 
    total_l1 = []
    total_ = [] 
    total_dhr_err = []
    total_dsr_err = []
    
    with torch.no_grad(): 
        for i, batch in enumerate(val_loader): 
            lr, hr = batch["lr"], batch["hr"]
            lr = lr.to(config.device)
            hr = hr.to(config.device)
            bs = lr.shape[0]
            true_labels = torch.full([bs, 1], 1.0, dtype=lr.dtype, device=config.device)
            fake_labels = torch.full([bs, 1], 0.0, dtype=lr.dtype, device=config.device)
            sr = g_net(lr)
            # save to folder
            # if i == 0: 
            #     save_image_to_folder(lr[:5]*255.0, sr[:5]*255.0, hr[:5] * 255.0, epoch, config.experiment_name)

            l1_error = l1_criterion(sr, hr)
            psnr = compute_psnr(sr, hr)
            total_psnr.append(psnr.item())
            percept = percept_criterion(sr, hr)
            total_percept.append(percept.item())
            l1_err = l1_criterion(sr, hr)
            total_l1.append(l1_err.item())
            
            sr_pred = d_net(sr) 
            hr_pred = d_net(hr)
            # relativistic losses 

            hr_realistic_pred = torch.nn.functional.sigmoid(hr_pred - sr_pred.mean(axis=0))
            d_hr_error = adv_criterion(hr_realistic_pred, true_labels.detach()) 
            total_dhr_err.append(d_hr_error.item())
            sr_realistic_pred = torch.nn.functional.sigmoid(sr_pred - hr_pred.mean(axis=0))
            d_sr_error = adv_criterion(sr_realistic_pred, fake_labels.detach()) 
            total_dsr_err.append(d_sr_error.item())

            print("val Error D HR = ", d_hr_error)
            print("val Error D SR = ", d_sr_error)
            print("Val l1_loss = ", l1_error.item())
            print("Val psnr = ", psnr.item())
            print("Val l1 loss = ", l1_err.item())
            print("Val percpt loss = ", percept.item())

        mean_psnr = np.array(total_psnr).mean()
        mean_percept= np.array(total_percept).mean()
        mean_l1 = np.array(total_l1).mean()
        mean_dhr_err = np.array(total_dhr_err).mean()
        mean_dsr_err = np.array(total_dsr_err).mean()

        writer.add_scalar("GAN_MODE/Validation/psnr", mean_psnr, epoch)
        writer.add_scalar("GAN_MODE/Validation/G_l1_loss", mean_l1, epoch)
        writer.add_scalar("GAN_MODE/Validation/G_Percept", mean_percept, epoch)
        writer.add_scalar("GAN_MODE/Validation/D_err_HR", mean_dhr_err, epoch)
        writer.add_scalar("GAN_MODE/Validation/D_err_SR", mean_dsr_err, epoch)

    return mean_psnr


def main(): 

    print("--- Load Dataset ---")
    train_loader, val_loader = get_dataloader()
    print("--- Load Dataset ---", "\n")

    print("--- Load Models ---")
    g_net, d_net = get_models()
    print("--- Load Models ---", "\n")

    print("--- Load criterions ---")
    percceptual_criterion, adv_criterion, l1_criterion = get_criterions()
    print("--- Load criterions ---", "\n")

    print("--- Load optimziers ---")
    g_optim, g_scheduler, d_optim, d_scheduler = get_optimizers(g_net, d_net)
    print("--- Load optimziers : DONE ---")

    print("--- Load writer ---")
    writer = SummaryWriter(config.experiment_name)
    print("--- Load writer : DONE ---", "\n")

    max_psnr = 0.0

    for epoch in range(config.epochs): 
        print("TRAINING FOR EPOCH : ", epoch)
        train(
            g_net,
            d_net, 
            g_optim,
            d_optim,  
            g_scheduler,
            d_scheduler, 
            percceptual_criterion, 
            adv_criterion, 
            l1_criterion, 
            train_loader, 
            writer, 
            epoch
        ) 
        psnr = validate(g_net, d_net, percceptual_criterion, adv_criterion, l1_criterion, train_loader, writer, epoch)

        if max_psnr <= psnr: 
            print("update best psnr !")
            max_psnr = psnr
            # save weight
            if not os.path.exists(config.WEIGHT_PATH): 
                os.mkdir(config.WEIGHT_PATH)
                
            torch.save(g_net.state_dict(), config.g_weight_best)




if __name__ == "__main__": 
    main()