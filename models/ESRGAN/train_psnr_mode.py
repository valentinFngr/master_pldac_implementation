import numpy as np 
import torch
import torch.nn as nn
import config
from models import Discriminator, Generator, PerceptualLoss  
from dataset import ESRGANDataset
from script import split_train_to_val, save_image_to_folder
from torch.utils.data import DataLoader
from utils import compute_psnr
from torch.utils.tensorboard import SummaryWriter
import os 

# fix image issues : 
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True




def get_models(): 
    """
        return models 
    """

    g_net = Generator().to(config.device) 

    if config.g_weight_path: 
        g_net.load_state_dict(torch.load(config.g_psnr_weight))

    return g_net 


def get_criterions(): 
    """
        return perpectual criterion, adversarial criterion, l1 criterion
    """
    l1 = nn.L1Loss().to(config.device) 
    return l1


def get_dataloader(): 
    
    split_train_to_val(config.val_size)

    train_dataset = ESRGANDataset(config.hr_size, config.lr_size, folder=config.TRAIN_PATH)
    test_dataset = ESRGANDataset(config.hr_size, config.lr_size, folder=config.VAL_PATH)

    print(f"Found {len(train_dataset)} data points in Train")
    print(f"Found {len(test_dataset)} data points in Test")

    train_data = DataLoader(
        train_dataset, 
        batch_size=config.batch_size, 
        shuffle=True
    )

    test_data = DataLoader(
        test_dataset, 
        batch_size=config.batch_size, 
        shuffle=True
    )

    return train_data, test_data


def get_optimizers(g_net): 
    g_optim = torch.optim.Adam(g_net.parameters(), lr=config.learning_rate, betas=(0.9, 0.999))    
    scheduler = torch.optim.lr_scheduler.StepLR(g_optim, step_size=1e4, gamma=0.5, verbose=False)

    return g_optim, scheduler


def validate(g_net, l1_criterion, val_loader, writer, epoch): 
    """
        validation step 
    """ 
    total_psnr = []
    with torch.no_grad(): 
        for i, batch in enumerate(val_loader): 
            lr, hr = batch["lr"], batch["hr"]
            lr = lr.to(config.device)
            hr = hr.to(config.device)
            bs = lr.shape[0]
            sr = g_net(lr)
            # save to folder
            if i == 0: 
                save_image_to_folder(lr[:5]*255.0, sr[:5]*255.0, hr[:5]*255.0, epoch, config.experiment_name)

            l1_error = l1_criterion(sr, hr)
            psnr = compute_psnr(sr, hr)
            total_psnr.append(psnr.item())
            print("Val l1_loss = ", l1_error.item())
            print("Val psnr = ", psnr.item())
    
        mean_psnr = np.array(total_psnr).mean()
        print("Mean PSNR = ", mean_psnr)
        writer.add_scalar("PSNR_MODE/Validation/l1_loss", l1_error.item(), epoch)
        writer.add_scalar("PSNR_MODE/Validation/psnr", mean_psnr, epoch)
    return mean_psnr
        


def train(g_net, g_optim, scheduler, l1_criterion, train_loader, writer, epoch):
    """
        Train the generator in pnsr mode
    """
    g_net.train()
    for i, batch in enumerate(train_loader): 
        
        lr, hr = batch["lr"], batch["hr"]
        lr = lr.to(config.device)
        hr = hr.to(config.device)
        bs = lr.shape[0]

        # train discriminator
        g_net.zero_grad()
        sr = g_net(lr)
        l1_error = l1_criterion(sr, hr)
        l1_error.backward()
        g_optim.step()
        
        scheduler.step()

        if i % 50 == 0: 
            print(f"[{i}/{len(train_loader)}] Train l1_loss = ", l1_error.item())
            print()

        writer.add_scalar("PSNR_MODE/Train/l1_loss", l1_error.item(), epoch*len(train_loader) + i + 1)


    return 

def main(): 

    print("--- Load Dataset ---")
    train_loader, val_loader = get_dataloader()
    print("--- Load Dataset ---", "\n")

    print("--- Load Models ---")
    g_net = get_models()
    print("--- Load Models ---", "\n")

    print("--- Load criterions ---")
    l1_criterion = get_criterions()
    print("--- Load criterions ---", "\n")

    print("--- Load optimizers ---")
    g_optim, scheduler = get_optimizers(g_net)
    print("--- Load optimizers ---", "\n")

    print("--- Load writer ---")
    writer = SummaryWriter(config.experiment_name)
    print("--- Load writer : DONE ---", "\n")


    max_psnr = 0.0

    for epoch in range(config.epochs): 
        print(f"Epoch : {epoch} ")
        train(g_net, g_optim, scheduler, l1_criterion, train_loader, writer, epoch)
        psnr = validate(g_net, l1_criterion, val_loader, writer, epoch)
        if max_psnr <= psnr: 
            print("update best psnr !")
            max_psnr = psnr
            # save weight
            if not os.path.exists(config.WEIGHT_PATH): 
                os.mkdir(config.WEIGHT_PATH)
                
            torch.save(g_net.state_dict(), config.g_psnr_best)

if __name__ == "__main__": 
    main()