import os 
import torch


device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print("Detected device : ", device)

# data 
DATA_PATH = "/Vrac"
# DATA_PATH = "/home/valentin/Desktop/master_thesis/data/"

TRAIN_PATH = os.path.join(DATA_PATH, "FACE_TRAIN")
VAL_PATH = os.path.join(DATA_PATH, "FACE_VAL")
TEST_PATH = ""
# general path weight dir
WEIGHT_PATH = os.path.join(os.getcwd(), "weights")


# result path 
RESULT_PATH = os.path.join(os.getcwd(), "results")
VAL_RESULT_PATH = os.path.join(RESULT_PATH, "validation")
PSNR_VAL_PATH = os.path.join(VAL_RESULT_PATH, "psnr_oriented")
GAN_VAL_PATH = os.path.join(VAL_RESULT_PATH, "gan")


# transforomation 
hr_size = 128
upscale_factor = 4
lr_size = hr_size // upscale_factor

# training 
epochs = 100
learning_rate = 0.001
learning_rate_decay = 2 
lambda_coeff = 0.001
l1_coeff = 1e-3
lrelu_slope = 0.2
rescaling_factor = 1
nb_rrdb_blocks = 16
residual_scaling = 0.2 # for blocks in generator
batch_size = 16
val_size = 0.2
interpolation_factor = 0.2

experiment_name = f"runs/faces_lr={learning_rate}_lambda={lambda_coeff}_l1_coeff={l1_coeff}_vggcoef={rescaling_factor}"


# folder to load weight from
g_weight_path = ""
d_weight_path = ""

# folder to save weight to
g_weight_best = os.path.join(WEIGHT_PATH, f"best_gan_generator_{experiment_name.split('/')[1]}")
d_weight_best = os.path.join(WEIGHT_PATH, f"best_gan_discriminator_{experiment_name.split('/')[1]}")

# folder to load best_psnr
g_psnr_weight = ""
# folder to save best psnr
g_psnr_best = os.path.join(WEIGHT_PATH, f"best_psnr_generator_{experiment_name.split('/')[1]}")
