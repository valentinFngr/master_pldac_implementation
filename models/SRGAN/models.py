import torch 
import config
import torchvision
import os
# os.environ['TORCH_HOME'] = '/Vrac/vgg_torch'
# print(os.environ['TORCH_HOME'])


class ResidualBlock(torch.nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Deep Residual Learning for Image Recognition
    *    Author: Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    *    Date: 2015
    *    Paper version: v1
    *    Availability: https://arxiv.org/abs/1512.03385
    *
    ***************************************************************************************/
    """
    def __init__(self, in_chan): 
        super(ResidualBlock, self).__init__()
        self.in_chan = in_chan
        self.conv1 = torch.nn.Conv2d(in_chan, 64, 3, stride=1, padding=1, bias=False)
        self.bn1 = torch.nn.BatchNorm2d(64) 
        self.prelu1 = torch.nn.PReLU()
        
        self.conv2 = torch.nn.Conv2d(in_chan, 64, 3, stride=1, padding=1, bias=False)
        self.bn2 = torch.nn.BatchNorm2d(64) 
        
        
    def forward(self, x): 
        
        # no copy needed
        
        out = self.prelu1(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        result = out + x
        return result
    

class DBlock(torch.nn.Module): 
    
    def __init__(self, in_chan, out_chan, stride=1): 
        self.in_chan = in_chan 
        self.out_chan = out_chan
        self.stride = stride
        super(DBlock, self).__init__()
        self.conv = torch.nn.Conv2d(in_chan, out_chan, 3, stride=stride, bias=False)
        self.bn = torch.nn.BatchNorm2d(out_chan)
        self.lrelu = torch.nn.LeakyReLU(config.slope, inplace=True)
        
        
    def forward(self, x): 
        
        output = self.lrelu(self.bn(self.conv(x)))
        return output
                              
        
class PixelShuffleBlock(torch.nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Real-Time Single Image and Video Super-Resolution Using an Efficient Sub-Pixel Convolutional Neural Network
    *    Author: Wenzhe Shi, Jose Caballero1 et al.
    *    Date: 2016
    *    Paper version: v2
    *    Availability: https://arxiv.org/pdf/1609.05158.pdf
    *
    ***************************************************************************************/
    """
    def __init__(self, in_chan, out_chan): 
        self.in_chan = in_chan
        self.out_chan = out_chan
        super(PixelShuffleBlock, self).__init__()
        
        self.conv = torch.nn.Conv2d(in_chan, out_chan, 3, stride=1, padding=0, bias=False)
        self.pixelshuffler = torch.nn.PixelShuffle(2)
        self.prelu = torch.nn.PReLU()
        
    def forward(self, x): 
        output = self.conv(x)
        output = self.prelu(self.pixelshuffler(output))
        return output



class Generator(torch.nn.Module): 
    """
    ***************************************************************************************/
    *    Title: Photo-Realistic Single Image Super-Resolution Using a Generative Adversaria Network
    *    Author: Christian Ledig, Lucas Theis et al.
    *    Date: 2017
    *    Paper version: v5
    *    Availability: https://arxiv.org/pdf/1609.04802.pdf
    *
    ***************************************************************************************/
    """
    def __init__(self): 
        super(Generator, self).__init__() 
        
        self.conv1 = torch.nn.Conv2d(3, 64, 3, stride=1, padding=3, dilation=1, bias=False)
        self.prelu1 = torch.nn.PReLU()
        
        # very deep 
        self.residual_blocks = torch.nn.Sequential(*[
            ResidualBlock(64).to(config.device) for _ in range(16)
        ])
        
        self.conv2 = torch.nn.Conv2d(64, 64, 3, stride=1, padding=1, bias=False)
        self.bn1 = torch.nn.BatchNorm2d(64)
        
        
        self.pxshuffle1 = PixelShuffleBlock(64, 256).to(config.device)
        self.pxshuffle2 = PixelShuffleBlock(64, 256).to(config.device)
        
        self.conv3 = torch.nn.Conv2d(64, 3, 5, stride=1, padding=0, bias=False)
        self.tanh = torch.nn.Tanh()
        

    def forward(self, x): 
        out1 = self.prelu1(self.conv1(x))
        out2 = self.residual_blocks(out1)
        out3 = self.bn1(self.conv2(out2))
        out3 = out3 + out1

        
        out4 = self.pxshuffle1(out3)
        out5 = self.pxshuffle2(out4)
        out6 = self.tanh(self.conv3(out5))
        
        # super resolution image
        assert out6.shape[1:] == (3, config.hr_size, config.hr_size)
        return out6
    
    
class Discriminator(torch.nn.Module): 
    
    def __init__(self): 
        super(Discriminator, self).__init__()
        self.conv1 = torch.nn.Conv2d(3, 32, 9, stride=1, padding=1, bias=False)
        self.lrelu1 = torch.nn.LeakyReLU(config.slope, inplace=True)
        
        self.d_modules = torch.nn.Sequential(*[
            DBlock(32, 32, 2), 
            DBlock(32, 64, 1), 
            DBlock(64,64, 2), 
            DBlock(64, 128, 1), 
            DBlock(128, 128, 2), 
        ])
        
        self.fc1 = torch.nn.Linear(2048, 1024)
        self.lrelu2 = torch.nn.LeakyReLU(config.slope, inplace=True)
        self.fc2 = torch.nn.Linear(1024, 1)
        self.sigmoid = torch.nn.Sigmoid()
            
    def forward(self, x): 
        out1 = self.lrelu1(self.conv1(x))
        out1 = out1 + torch.randn(out1.shape, device=torch.device("cuda"))
        out2 = self.d_modules(out1).view(out1.shape[0], -1)
        out2 = out2 + torch.randn(out2.shape, device=torch.device("cuda"))
        out3 = self.fc1(out2) 
        out3 = out3 + torch.randn(out3.shape, device=torch.device("cuda"))
        out4 = self.lrelu2(out3)
        out4 = out4 + torch.randn(out4.shape, device=torch.device("cuda"))
        out5 = self.fc2(out4)
        out5 = out5 + torch.randn(out5.shape, device=torch.device("cuda"))
        out6 = self.sigmoid(out5)
        out6 = out6 
        return out6
        

class ContentLoss(torch.nn.Module): 
    """
        Implementation of the content loss from the SRGAN paper
        We load a pre trained vgg19 and freeze all weights.
        We use the original high resolution image with the super resolution (generated)
        image and compare the mse error between their output when fed to the pre trained vgg19
    """
    def __init__(self):
        super(ContentLoss, self).__init__()
        # build pretrained vgg 
        layers = torch.nn.Sequential(*list(torchvision.models.vgg19(pretrained=True).eval().features.children())[:34])            
        # freeze all weights
        for param in layers.parameters():
            param.requires_grad = False
            
        self.layers = layers
        self.normalizations =  torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

        # mse criterion 
        self.mse_criterion = torch.nn.MSELoss()
        
    def forward(self, hr, sr): 
        hr_norm = self.normalizations(hr) 
        sr_norm = self.normalizations(sr)
        vgg_hr = self.layers(hr_norm) 
        vgg_sr = self.layers(sr_norm)
        result = self.mse_criterion(vgg_hr, vgg_sr) * config.rescaling_factor
        return result
