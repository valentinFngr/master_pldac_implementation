import torch 
from models import Generator, Discriminator, ContentLoss
import config 
from data import SRGANDataset, my_collate
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
import os
from utils import compute_psnr
import numpy as np

def get_data(): 
    """"
        Return the train and test data loader
    """

    train_dataset = SRGANDataset(config.hr_size, config.lr_size, folder=config.TRAIN_PATH)
    test_dataset = SRGANDataset(config.hr_size, config.lr_size, folder=config.VAL_PATH,max_size=6000)

    print(f"Found {len(train_dataset)} data points in Train")
    print(f"Found {len(test_dataset)} data points in Test")

    train_data = DataLoader(
        train_dataset, 
        batch_size=config.batch_size, 
        shuffle=True, 
        collate_fn=my_collate
    )

    test_data = DataLoader(
        test_dataset, 
        batch_size=config.batch_size, 
        shuffle=True, 
        collate_fn=my_collate
    )

    return train_data, test_data



def get_models(): 
    """
        return the generator and discriminator models
    """
    g_net = Generator().to(config.device)
    d_net = Discriminator().to(config.device)

    if config.g_weight_path: 
        g_net.load_state_dict(torch.load(config.g_weight_path))
    if config.d_weight_path: 
        d_net.load_state_dict(torch.load(config.d_weight_path))
    
    return g_net, d_net



def get_optimizers(g_net, d_net): 
    """
        return the generator and discriminator optimizers 
    """
    g_optim = torch.optim.Adam(g_net.parameters(), lr=config.learning_rate, betas=(config.beta, 0.999))
    d_optim = torch.optim.Adam(d_net.parameters(), lr=config.learning_rate, betas=(config.beta, 0.999))

    return g_optim, d_optim

def get_criterions(): 
    content_loss = ContentLoss().to(config.device)
    bce_criterion = torch.nn.BCELoss().to(config.device)

    return content_loss, bce_criterion


def train(g_net, d_net, g_optim, d_optim, bce_criterion, perceptual_criterion, train_loader, writer, epoch): 
    """
        Train the generator and the discriminator for a single epoch
    """

    torch.autograd.set_detect_anomaly(True)
    g_net.train()
    d_net.train()

    for i, batch in enumerate(train_loader): 
        
        lr, hr = batch["lr"], batch["hr"]
        lr = lr.to(config.device)
        hr = hr.to(config.device)
        bs = lr.shape[0]
        

        # train discriminator
        sr = g_net(lr)
        
        for p in d_net.parameters():
            p.requires_grad = True
        
        label = torch.full((bs, 1), 1, device=config.device, dtype=torch.float32)
        d_optim.zero_grad()
        real_pred = d_net(hr)
                
        real_loss = bce_criterion(real_pred,label)
        real_loss.backward()
        # change label 
        label.fill_(0.0)
        fake_pred = d_net(sr.detach())
        fake_loss = bce_criterion(fake_pred, label)
        fake_loss.backward()
        d_loss = fake_loss + real_loss
        d_optim.step()
        
        for p in d_net.parameters():
            p.requires_grad = False
        
        with torch.no_grad():
            p_real = real_pred.mean(axis=0)
            p_fake = fake_pred.mean(axis=0)
        
        # train generator
        
        # change label 
        g_net.zero_grad()
        label.fill_(1.)
        sr_preds = d_net(sr)
        adv_loss = bce_criterion(sr_preds, label)
        adv_loss.backward(retain_graph=True)
        content_criterion = perceptual_criterion(hr.detach(), sr)
        content_criterion.backward()
        g_loss = adv_loss + content_criterion
        g_optim.step()
        
        with torch.no_grad():
            p_fake_trick = sr_preds.mean(axis=0)
    
        # monitoring
        writer.add_scalar("Loss/d_loss", d_loss.item(), epoch*len(train_loader) + i + 1)
        writer.add_scalar("Loss/g_loss", g_loss.item(), epoch*len(train_loader) + i + 1)
        writer.add_scalar("Loss/perceptual_loss", content_criterion.item(), epoch*len(train_loader) + i + 1)
        writer.add_scalar("Probas/D(real)", p_real.item(), epoch*len(train_loader) + i + 1)
        writer.add_scalar("Probas/D(Fake)", p_fake.item(), epoch*len(train_loader) + i + 1)
        writer.add_scalar("Probas/D(Fake)2", p_fake_trick.item(), epoch*len(train_loader) + i + 1)


def validate(
    g_net,
    d_net, 
    adv_criterion,
    percept_criterion, 
    val_loader, 
    writer, 
    epoch
    ): 

    total_psnr = []
    total_percept = [] 
    total_ = [] 
    total_dhr_err = []
    total_dsr_err = []

    g_net.eval()
    d_net.eval()

    with torch.no_grad(): 
        for i, batch in enumerate(val_loader): 
            lr, hr = batch["lr"], batch["hr"]
            lr = lr.to(config.device)
            hr = hr.to(config.device)
            bs = lr.shape[0]
            true_labels = torch.full([bs, 1], 1.0, dtype=lr.dtype, device=config.device)
            fake_labels = torch.full([bs, 1], 0.0, dtype=lr.dtype, device=config.device)
            sr = g_net(lr)
            sr_pred = np.where(d_net(sr).cpu().numpy() >= 0.5, 1, 0)
            hr_pred = np.where(d_net(hr).cpu().numpy()>= 0.5, 1, 0)

            psnr = compute_psnr(sr, hr)
            total_psnr.append(psnr.item())
            percept = percept_criterion(sr, hr)
            total_percept.append(percept.item())

            d_hr_error = np.where(hr_pred == true_labels, 1., 0.).mean()
            total_dhr_err.append(d_hr_error.item())

            d_sr_error = np.where(sr_pred == fake_labels, 1., 0.).mean()
            total_dsr_err.append(d_sr_error.item())

            mean_psnr = np.array(total_psnr).mean()
            mean_percept= np.array(total_percept).mean()
            mean_dhr_err = np.array(total_dhr_err).mean()
            mean_dsr_err = np.array(total_dsr_err).mean()

            writer.add_scalar("Validation/psnr", mean_psnr, epoch)
            writer.add_scalar("Validation/G_Percept", mean_percept, epoch)
            writer.add_scalar("Validation/D(HR)_acc", mean_dhr_err, epoch)
            writer.add_scalar("Validation/D(SR)_acc", mean_dsr_err, epoch)

    return mean_psnr






if __name__ == "__main__": 
    
    print("--- Load Dataset ---")
    train_loader, val_loader = get_data()
    print("--- Load Dataset ---", "\n")

    print("--- Load Models ---")
    g_net, d_net = get_models()
    print("--- Load Models ---", "\n")

    print("--- Load criterions ---")
    perceptual_criterion, adv_criterion = get_criterions()
    print("--- Load criterions ---", "\n")

    print("--- Load optimziers ---")
    g_optim, d_optim = get_optimizers(g_net, d_net)
    print("--- Load optimziers : DONE ---")

    print("--- Load writer ---")
    writer = SummaryWriter(config.experiment_name)
    print("--- Load writer : DONE ---", "\n")

    max_psnr = 0.0

    for epoch in range(config.epochs): 
        print("TRAINING FOR EPOCH : ", epoch)
        train(
            g_net,
            d_net, 
            g_optim,
            d_optim,   
            adv_criterion,
            perceptual_criterion,
            train_loader, 
            writer, 
            epoch
        ) 
        psnr = validate(g_net, d_net, adv_criterion, perceptual_criterion, train_loader, writer, epoch)

        if max_psnr <= psnr: 
            print("update best psnr !")
            max_psnr = psnr
            # save weight
            if not os.path.exists(config.WEIGHT_PATH): 
                os.mkdir(config.WEIGHT_PATH)
                
            torch.save(g_net.state_dict(), config.g_weight_best)
            torch.save(d_net.state_dict(), config.d_weight_best)
        torch.save(g_net.state_dict(), config.g_weight_checkpoint)
        torch.save(d_net.state_dict(), config.d_weight_checkpoint)