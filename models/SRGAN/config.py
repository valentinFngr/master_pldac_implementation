import os 
import torch

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print("Detected device : ", device)


# data 

# DATA_PATH = "/home/valentin/Desktop/master_thesis/data"
DATA_PATH = "/home/valentin/Desktop/master_thesis/data/"
TRAIN_PATH = os.path.join(DATA_PATH, "FACE_TRAIN")
VAL_PATH = os.path.join(DATA_PATH, "FACE_VAL")
print(VAL_PATH)
WEIGHT_PATH = os.path.join(os.getcwd(), "weights")

# transforomation 
hr_size = 64 
upscale_factor = 4
lr_size = hr_size // upscale_factor

# training 
adv_coeff = 1e-3 # coef in front of the adversarial loss
learning_rate = 0.0002
beta = 0.9 
batch_size = 16 
slope = 0.2 
epochs = 200
rescaling_factor = 1

experiment_name = f"runs/faces_lr={learning_rate}_adv_coeff={adv_coeff}_batch_size={batch_size}_croping_noise_added_before_activ"

# pre trained weights 

resume = False
g_weight_path = ""
d_weight_path = ""

g_weight_checkpoint = os.path.join(WEIGHT_PATH, f"checkpoint_generator_{experiment_name.split('/')[1]}")
d_weight_checkpoint = os.path.join(WEIGHT_PATH, f"checkpoint_discriminator_{experiment_name.split('/')[1]}")

g_weight_best = os.path.join(WEIGHT_PATH, f"best_generator_{experiment_name.split('/')[1]}")
d_weight_best = os.path.join(WEIGHT_PATH, f"best_discriminator_{experiment_name.split('/')[1]}")
