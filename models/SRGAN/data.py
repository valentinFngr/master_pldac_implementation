from torch.utils.data import Dataset
import glob
from torchvision import transforms
from PIL import Image
import numpy as np
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from torch.utils.data.dataloader import default_collate


def my_collate(batch):
    batch = list(filter(lambda x:x is not None, batch))
    return default_collate(batch)


class SRGANDataset(Dataset): 
    """
        A custom class to extract data
    """
    def __init__(self, hr_size, lr_size, folder, convert_to_rgb=True, max_size=None): 
        super(SRGANDataset, self).__init__() 
        self.hr_size = hr_size 
        self.lr_size = lr_size
        self.folder = folder 
    
        self.hr_transform = transforms.Compose([
            transforms.Resize((hr_size, hr_size)), 
            transforms.CenterCrop(hr_size), 
            transforms.ToTensor(), 
            transforms.Normalize((0.5, 0.5, 0.5), (0.5,0.5, 0.5))
        ])
        
        self.lr_transform = transforms.Compose([
            transforms.Resize((lr_size, lr_size)), 
            transforms.CenterCrop(lr_size), 
            transforms.ToTensor(), 
            transforms.Normalize((0.5, 0.5, 0.5), (0.5,0.5, 0.5))
        ])

        if max_size: 
            self.files = sorted(glob.glob(folder + "/*.*"))[:max_size]
        else: 
            self.files = sorted(glob.glob(folder + "/*.*"))            
        
    def __getitem__(self, idx): 
        
        img_name = self.files[idx] 
        try: 
            image_obj = Image.open(img_name)
            image_obj = image_obj.convert("RGB")
            hr = self.hr_transform(image_obj)
            lr = self.lr_transform(image_obj)
            
            return {
                "hr" : hr, 
                "lr" : lr
            }
        except Exception as e:
            print(e)
            return None    
    def __len__(self): 
        return len(self.files)

    
    
    