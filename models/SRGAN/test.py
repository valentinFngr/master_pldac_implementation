from models import Generator
import torch 
import config 
from data import SRGANDataset, my_collate
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
import os
from utils import compute_psnr
import numpy as np
from torch.utils.data import DataLoader
import torchvision
import matplotlib.pyplot as plt 



def get_models(): 
    """
        return the generator and discriminator models
    """
    g_net = Generator().to(config.device)

    if config.g_weight_best: 
        g_net.load_state_dict(torch.load(config.g_weight_best))
    
    return g_net



def get_data(): 
    """"
        Return the train and test data loader
    """

    test_dataset = SRGANDataset(config.hr_size, config.lr_size, folder=config.VAL_PATH)

    print(f"Found {len(test_dataset)} data points in Test")


    test_data = DataLoader(
        test_dataset, 
        batch_size=config.batch_size, 
        shuffle=True
    )

    return test_data



def test(g_net, test_loader):

    g_net.eval()

    with torch.no_grad(): 
         
        for i, batch in enumerate(test_loader): 
            lr, hr = batch["lr"], batch["hr"]
            lr = lr.to(config.device)
            hr = hr.to(config.device)
            bs = lr.shape[0]
            sr = g_net(lr) 
            plt.figure(1)
            grid = torchvision.utils.make_grid(sr.cpu())
            grid = np.transpose(grid, (1,2, 0))
            plt.imshow(grid)
            plt.figure(2)
            grid = torchvision.utils.make_grid(hr.cpu())
            grid = np.transpose(grid, (1,2, 0))
            plt.imshow(grid)
            plt.show()



if __name__ == "__main__": 

    print("--- Load Dataset ---")
    val_loader = get_data()
    print("--- Load Dataset ---", "\n")

    print("--- Load Models ---")
    g_net = get_models()
    print("--- Load Models ---", "\n")
    

    test(g_net, val_loader)